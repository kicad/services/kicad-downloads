# This is a stub to be able to enable hot reloading for debug
from app.main import application

import uvicorn

if __name__ == '__main__':
    uvicorn.run("serve:application", host='0.0.0.0', port=8000, reload=True)