import json
import pytest

from ..support.json import assert_valid_schema

def test_platform_config() -> None:
    f = open('data/platforms.json',)
    json_data = json.load(f)
    assert_valid_schema(json_data, 'platforms.json')
    f.close()