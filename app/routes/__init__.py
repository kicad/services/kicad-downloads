from starlette.routing import Mount, Route, Router

from fastapi.responses import JSONResponse, \
    PlainTextResponse, \
    RedirectResponse
    
from starlette.requests import Request
from starlette.responses import Response

import gzip
import prometheus_client as prom
from prometheus_client import multiprocess
import os
import secrets
import app.config as config

from typing import Annotated
from fastapi import Depends, APIRouter, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials

root_router = APIRouter()
security = HTTPBasic()


@root_router.get("/")
async def homebounce(request: Request) -> Response:
    url = request.url_for("get_kicad_index")
    return RedirectResponse(url)


def get_metrics_auth( credentials: Annotated[HTTPBasicCredentials, Depends(security)] ):
    if config.METRICS_USER == "" and config.METRICS_PASS == "":
        return

    current_username_bytes = credentials.username.encode("utf8")
    correct_username_bytes = config.METRICS_USER.encode("utf8")
    is_correct_username = secrets.compare_digest(
        current_username_bytes, correct_username_bytes
    )
    current_password_bytes = credentials.password.encode("utf8")
    correct_password_bytes = config.METRICS_PASS.encode("utf8")
    is_correct_password = secrets.compare_digest(
        current_password_bytes, correct_password_bytes
    )
    if not (is_correct_username and is_correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Basic"},
        )


@root_router.get("/metrics", dependencies = [Depends(get_metrics_auth)])
async def get_metrics(request: Request) -> Response:
    """Endpoint that serves Prometheus metrics."""

    ephemeral_registry = prom.REGISTRY
    if "PROMETHEUS_MULTIPROC_DIR" in os.environ:
        ephemeral_registry = prom.CollectorRegistry()
        multiprocess.MultiProcessCollector(ephemeral_registry)

    if "gzip" in request.headers.get("Accept-Encoding", ""):
        resp = Response(
            content=gzip.compress(prom.generate_latest(ephemeral_registry))
        )
        resp.headers["Content-Type"] = prom.CONTENT_TYPE_LATEST
        resp.headers["Content-Encoding"] = "gzip"
    else:
        resp = Response(content=prom.generate_latest(ephemeral_registry))
        resp.headers["Content-Type"] = prom.CONTENT_TYPE_LATEST

    return resp