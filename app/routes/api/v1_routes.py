from fastapi import FastAPI
from fastapi.requests import Request
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse, \
    Response

from app.models.api.v1 import *

import app.data as appdata
import semver

import datetime
import os
import re

from prometheus_client import CollectorRegistry, Counter, multiprocess, REGISTRY

if "PROMETHEUS_MULTIPROC_DIR" in os.environ:
    registry = CollectorRegistry()
    multiprocess.MultiProcessCollector(registry)
else:
    registry = REGISTRY

update_hits_counter = Counter('kidl_update_requests', 'Update Requests', ['date','platform', 'arch','current_version','lang'], registry=registry)
update_hits_counter_rolling = Counter('kidl_update_requests_rolling', 'Rolling Update Requests', ['platform', 'arch','current_version','lang'], registry=registry)

app = FastAPI()

@app.route('/')
async def index(request: Request):
    content = []
    return JSONResponse(content)

def record_hit(requestModel: UpdateGetRequestModel, request: Request):
    last_check: datetime.datetime | None = None
    if requestModel.last_check != "":
        last_check = datetime.datetime.fromisoformat(requestModel.last_check)

    count_cutoff = datetime.datetime.today().replace(minute=0, hour=0, second=0, microsecond=0, day=1)

    if( last_check == None or last_check < count_cutoff ):
        date = datetime.datetime.now().strftime('%Y-%m')
        update_hits_counter.labels(date, requestModel.platform, requestModel.arch, requestModel.current_version, requestModel.lang).inc()

    update_hits_counter_rolling.labels(requestModel.platform, requestModel.arch, requestModel.current_version, requestModel.lang).inc()

@app.post('/update')
async def post_update(requestModel: UpdateGetRequestModel, request: Request) -> Response:
    current_version = requestModel.current_version

    try:
        client_release = semver.Version.parse(current_version)
    except ValueError:
        return Response(status_code=404)

    client_release = semver.Version.parse(current_version)
    
    # testing builds use a git-describe generated trailer i.e.
    # -<commitcountfromtag>-g<hash>, the default is also -unknown
    # lets only support a prelease tag of -rc<#> for now
    if client_release.prerelease:
        valid_prerelease_regex = re.compile(r"rc\d+")
        if not valid_prerelease_regex.match(client_release.prerelease):
            client_release = client_release.replace(prerelease = '')

    configured_release = semver.Version.parse(appdata.status['latest_update_release'])

    record_hit(requestModel, request)

    if client_release >= configured_release:
        return Response(status_code=204)

    if not appdata.status['latest_update_release'] in appdata.releases:
        return Response(status_code=500)
    
    release_info = appdata.releases[appdata.status['latest_update_release']]

    # check if the platform and arch have a configured download which indicates a update really is available
    downloadInfo = None
    for download in release_info['downloads']:
        if download['os'] == requestModel.platform and download['arch'] == requestModel.arch:
            downloadInfo = download
            break
    
    if downloadInfo == None:
        return Response(status_code=204)
    
    if requestModel.platform == 'windows':
        downloads_url = 'https://www.kicad.org/download/windows/'
    elif requestModel.platform == 'macos':
        downloads_url = 'https://www.kicad.org/download/macos/'
    else:
        downloads_url = 'https://www.kicad.org/download/linux/'

    responseObject = UpdateGetResponseModel(
        version=release_info['version'],
        release_date=release_info['release_date'],
        details_url=release_info['details_url'],
        downloads_url=downloads_url,
    )

    return JSONResponse(content=jsonable_encoder(responseObject))