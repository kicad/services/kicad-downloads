from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse, \
    PlainTextResponse, \
    RedirectResponse, \
    StreamingResponse, \
    FileResponse, \
    HTMLResponse, \
    Response
    
from fastapi.requests import Request

from operator import itemgetter
import urllib.parse

from prometheus_client import Counter

from app.models.s3 import S3Resource, S3APIAccess
from app.resources import templates
from app.config import KICAD_CERN_S3_ENDPOINT, KICAD_CERN_S3_BUCKET, KICAD_R2_ENDPOINT, KICAD_R2_KEY_ID, KICAD_R2_ACCESS_KEY, KICAD_R2_PUBLIC
import app.data
import app.s3
import os

cern_s3 : S3Resource = { 
                            'api_endpoint': KICAD_CERN_S3_ENDPOINT,
                            'api_mode': S3APIAccess.PUBLIC,
                            'bucket': KICAD_CERN_S3_BUCKET,
                            'public_base_url': urllib.parse.urljoin(KICAD_CERN_S3_ENDPOINT, KICAD_CERN_S3_BUCKET)
                        }


cfr2_s3 : S3Resource = { 
                            'api_endpoint': KICAD_R2_ENDPOINT,
                            'api_mode': S3APIAccess.PRIVATE,
                            'key_id': KICAD_R2_KEY_ID,
                            'access_key': KICAD_R2_ACCESS_KEY,
                            'bucket': KICAD_CERN_S3_BUCKET,
                            'public_base_url': KICAD_R2_PUBLIC
                        }


application = FastAPI()

from prometheus_client import CollectorRegistry, Counter, multiprocess, REGISTRY

if "PROMETHEUS_MULTIPROC_DIR" in os.environ:
    registry = CollectorRegistry()
    multiprocess.MultiProcessCollector(registry)
else:
    registry = REGISTRY

download_hits_counter = Counter('kidl_explore_downloads', 'Explore Downloads', ['platform', 'train','file'], registry=registry)


@application.route('/')
async def get_kicad_index(request):
    platforms = app.data.platforms

    return templates.TemplateResponse("files/index.html", 
                                                    {
                                                        "request": request,
                                                        "platforms": platforms
                                                    })


@application.route('/{platform}')
async def get_platform_page(request: Request) -> Response:
    platform = request.path_params['platform']

    if platform not in app.data.platforms:
        raise HTTPException(status_code=404, detail="Unknown platform")

    platform_config = app.data.platforms[platform]

    return templates.TemplateResponse("files/platform.html", 
                                                    {
                                                        "request": request,
                                                        "platform_key": platform,
                                                        "platform": platform_config, 
                                                    })

def __get_s3_resource( key: str ) -> S3Resource:
    if key == 'chcern':
        s3_resource = cern_s3
    elif key == 'r2':
        s3_resource = cfr2_s3
    else: 
        raise HTTPException(status_code=500, detail="Misconfigured s3 source")

    return s3_resource
    


@application.route('/{platform}/explore/{train}')
async def get_explore_view(request: Request) -> Response:
    platform = request.path_params['platform']
    train = request.path_params['train']

    if platform not in app.data.platforms:
        raise HTTPException(status_code=404, detail="Unknown platform")

    platform_config = app.data.platforms[platform]
    
    if train not in platform_config['explore']:
        raise HTTPException(status_code=404, detail="Unknown train")

    train_data = platform_config['explore'][ train ]

    s3_resource = __get_s3_resource(train_data['mirror'])

    files = await app.s3.fetch_s3_objects(train_data['prefix'], s3_resource)

    files = files.values()
    files = sorted(files, key=itemgetter('last_modified'), reverse=True)

    return templates.TemplateResponse("files/explore.html", 
                                                    {
                                                        "request": request,
                                                        "platform_key": platform,
                                                        "platform": platform_config, 
                                                        "train_key": train,
                                                        "train": train_data,
                                                        "files": files,
                                                    })


@application.route('/{platform}/explore/{train}/download/{filename}')
async def get_explore_download(request: Request) -> Response:
    
    platform = request.path_params['platform']
    train = request.path_params['train']

    if platform not in app.data.platforms:
        raise HTTPException(status_code=404, detail="Unknown platform")

    platform_config = app.data.platforms[platform]
    
    if train not in platform_config['explore']:
        raise HTTPException(status_code=404, detail="Unknown train")

    train_data = platform_config['explore'][ train ]

    s3_resource = __get_s3_resource(train_data['mirror'])

    files = await app.s3.fetch_s3_objects(train_data['prefix'], s3_resource)

    filename = request.path_params['filename']

    if filename not in files:
        raise HTTPException(status_code=404, detail="Unknown file")

    file = files[filename]
    url = urllib.parse.urljoin(s3_resource['public_base_url']+"/", file['key'])

    download_hits_counter.labels(platform, train, filename).inc()

    return RedirectResponse(url, status_code=307)