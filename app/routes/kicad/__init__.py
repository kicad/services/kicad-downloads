from fastapi import FastAPI
from starlette.routing import Mount, Route, Router
from fastapi.responses import JSONResponse, \
    PlainTextResponse, \
    RedirectResponse, \
    StreamingResponse, \
    FileResponse, \
    HTMLResponse

from .routes import application as kicad_routes

api_routes = FastAPI()
api_routes.mount('/', app=kicad_routes)