from starlette.config import Config
from starlette.datastructures import URL, Secret
import os

# Config will be read from environment variables and/or ".env" files.
_config = Config(".env")

DEBUG = _config('DEBUG', cast=bool, default=False)
TESTING = _config('TESTING', cast=bool, default=False)
DATABASE_URL = _config('DATABASE_URL', default='')

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
BASE_DIR = os.path.dirname(APP_ROOT)

KICAD_CERN_S3_ENDPOINT = _config('KICAD_CERN_S3_ENDPOINT', default='https://s3.cern.ch/')
KICAD_CERN_S3_BUCKET = _config('KICAD_CERN_S3_BUCKET', default='kicad-downloads')

KICAD_R2_ENDPOINT = _config('KICAD_R2_ENDPOINT', default='')
KICAD_R2_KEY_ID = _config('KICAD_R2_KEY_ID', default='')
KICAD_R2_ACCESS_KEY = _config('KICAD_R2_ACCESS_KEY', default='')
KICAD_R2_PUBLIC = _config('KICAD_R2_PUBLIC', default='')

REDIS_CN = _config('REDIS_CN', default='redis://localhost')

SENTRY_DSN = _config('SENTRY_DSN', default='')

METRICS_PUSHGATEWAY = _config('METRICS_PUSHGATEWAY', default='')
METRICS_PUSHGATEWAY_USER = _config('METRICS_PUSHGATEWAY_USER', default='')
METRICS_PUSHGATEWAY_PASS = _config('METRICS_PUSHGATEWAY_PASS', default='')

METRICS_USER = _config('METRICS_USER', default='')
METRICS_PASS = _config('METRICS_PASS', default='')