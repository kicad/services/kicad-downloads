from aiocache import Cache

cache : Cache = Cache(Cache.MEMORY)

async def get(key):
    return await cache.get(key)

async def set(key, value, ttl ):
    return await cache.set(key, value, ttl)
