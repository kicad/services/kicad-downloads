from datetime import datetime
from typing import TypedDict
from enum import Enum

class S3File(TypedDict):
    filename: str
    key: str
    last_modified: datetime 
    size: int

class S3APIAccess(Enum):
    PUBLIC = 0
    PRIVATE = 1

class S3Resource(TypedDict):
    api_endpoint: str
    mode: str
    key_id: str
    access_key: str
    api_mode: S3APIAccess
    bucket: str
    public_base_url: str
