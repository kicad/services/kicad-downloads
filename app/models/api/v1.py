from pydantic import BaseModel

class UpdateGetRequestModel(BaseModel):
    current_version: str
    lang: str
    last_check: str | None = None
    platform: str
    arch: str

class UpdateGetResponseModel(BaseModel):
    version: str
    release_date: str
    details_url: str
    downloads_url: str