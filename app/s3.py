import boto3
from botocore import UNSIGNED
from botocore.client import Config
from aiobotocore.session import get_session
from app.models.s3 import S3File, S3Resource, S3APIAccess
from typing import Dict
import app.cache

CACHE_LIFETIME=(60*30) # in seconds

async def fetch_s3_objects(prefix: str, resource: S3Resource) -> Dict[str, S3File]:
    session = get_session()

    cacheKey = f"{resource['api_endpoint']}{resource['bucket']}{prefix}"

    objects = await app.cache.get(cacheKey)

    if objects is None:
        if resource['api_mode'] == S3APIAccess.PUBLIC:
            configObj = config=Config(signature_version=UNSIGNED)
        else:
            configObj = None

        async with session.create_client('s3', config=configObj,
                                                endpoint_url=resource['api_endpoint'],
                                                aws_access_key_id=resource.get('key_id',''),
                                                aws_secret_access_key=resource.get('access_key','')) as s3_client:

            objects = {}

            try:
                response = await s3_client.list_objects_v2(Bucket=resource['bucket'], Prefix=prefix)
            except:
                return objects

            if 'Contents' in response:
                for item in response['Contents']:
                    filename = item['Key']
                    filename = filename.replace(prefix,"")

                    # a object can get accidentally created as the prefix only and end up an invalid file here so ignore it
                    if filename:
                        objects[filename] = S3File(filename=filename,
                                                        key=item['Key'],
                                                        last_modified=item['LastModified'], 
                                                        size=item['Size'])
            
            await app.cache.set(cacheKey, objects, CACHE_LIFETIME)

    return objects