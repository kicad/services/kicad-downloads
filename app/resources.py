from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
import humanize

templates = Jinja2Templates(directory='templates')
staticFiles = StaticFiles(directory='static')

def naturalsize_filter(text):
    return humanize.naturalsize(text)

def naturaldate_filter(text):
    return humanize.naturaldate(text)

def naturaltime_filter(text):
    return humanize.naturaltime(text)

templates.env.filters['naturalsize'] = naturalsize_filter
templates.env.filters['naturaldate'] = naturaldate_filter
templates.env.filters['naturaltime'] = naturaltime_filter