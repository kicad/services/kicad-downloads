import aiofiles
import os
import json
import app.config as config


platforms = {}
mirrors = {}
releases = {}
status = {}

async def __load_platforms():
    global platforms
    platforms = {}

    async with aiofiles.open(os.path.join(config.BASE_DIR, "data/platforms.json")) as f:
        contents = await f.read()
        
    platforms = json.loads(contents)

    return

async def __load_mirrors():
    global mirrors
    mirrors = {}

    async with aiofiles.open(os.path.join(config.BASE_DIR, "data/mirrors.json")) as f:
        contents = await f.read()

    mirrors = json.loads(contents)
    return

async def __load_status():
    global status
    status = {}

    async with aiofiles.open(os.path.join(config.BASE_DIR, "data/status.json")) as f:
        contents = await f.read()

    status = json.loads(contents)
    return

async def __load_releases():
    global releases
    releases = {}

    basePath = os.path.join(config.BASE_DIR, "data/releases")
    for filename in os.listdir(basePath):
        async with aiofiles.open(os.path.join(basePath, filename)) as f:
            contents = await f.read()
            jsonObject = json.loads(contents)
            releases[ jsonObject['version'] ] = jsonObject

    return

async def load():
    await __load_platforms()
    await __load_mirrors()
    await __load_status()
    await __load_releases()
    return