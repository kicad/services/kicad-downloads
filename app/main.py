from fastapi import FastAPI
from fastapi_utils.tasks import repeat_every

from app.routes.api import api_routes
from app.routes.kicad import kicad_routes
from app.routes import root_router
from app.resources import staticFiles, templates
import app.config as config
import app.data

import prometheus_client as prom


if config.SENTRY_DSN:
    import sentry_sdk

    sentry_sdk.init(
        dsn=config.SENTRY_DSN,
        traces_sample_rate=0.05,
    )

application = FastAPI()
application.debug = config.DEBUG

application.include_router( root_router )
application.mount('/static', staticFiles, name='static')
application.mount('/api', api_routes)
application.mount('/kicad', kicad_routes)

# Remove the default prom metrics they will just annoy us
prom.REGISTRY.unregister(prom.PROCESS_COLLECTOR)
prom.REGISTRY.unregister(prom.PLATFORM_COLLECTOR)
prom.REGISTRY.unregister(prom.GC_COLLECTOR)


@application.on_event('startup')
async def startup():
    await app.data.load()

@application.exception_handler(404)
async def not_found(request, exc):
    context = {"request": request}
    return templates.TemplateResponse("404.html", context, status_code=404)


@application.exception_handler(500)
async def server_error(request, exc):
    context = {"request": request}
    return templates.TemplateResponse("500.html", context, status_code=500)

def __metrics_push_auth_handler(url, method, timeout, headers, data):
    if config.METRICS_PUSHGATEWAY_USER != "":
        return prom.exposition.basic_auth_handler(url, method, timeout, headers, data, config.METRICS_PUSHGATEWAY_USER, config.METRICS_PUSHGATEWAY_PASS)
    else:
        return prom.exposition.default_handler(url, method, timeout, headers, data )

@application.on_event('startup')
@repeat_every(seconds=60 * 10) # 10 minutes
async def push_metrics() -> None:
    if config.METRICS_PUSHGATEWAY != "":
        prom.push_to_gateway(config.METRICS_PUSHGATEWAY, job='kicad-downloads', registry=prom.REGISTRY, handler=__metrics_push_auth_handler)