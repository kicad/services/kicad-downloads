FROM python:3.10-slim

# Setup our base environment
COPY ./docker/start.sh /start.sh
RUN chmod +x /start.sh

COPY ./docker/gunicorn_conf.py /gunicorn_conf.py
COPY ./docker/start-reload.sh /start-reload.sh
RUN chmod +x /start-reload.sh

# Now copy over the app
COPY . /app
WORKDIR /app/

ENV PYTHONPATH=/app
ENV PROMETHEUS_MULTIPROC_DIR="/tmp/prometheus-metrics"

RUN pip install --disable-pip-version-check --no-cache-dir poetry
RUN poetry config virtualenvs.create false && poetry install --no-root --without dev

RUN mkdir -m 777 -p "${PROMETHEUS_MULTIPROC_DIR}"

ENV APP_MODULE="app.main"
ENV PORT=8081

EXPOSE 8081

# use a different user as open shift wants non-root containers
# do it at the end here as it'll block our "root" commands to set the container up
USER 1000

# Run the start script, it will check for an /app/prestart.sh script (e.g. for migrations)
# And then will start Gunicorn with Uvicorn
CMD ["/start.sh"]